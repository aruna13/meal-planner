import { Injectable } from '@angular/core';

const DB_NAME = 'recipe-db';
const DB_VERSION = 1;
const OBJECT_STORE_NAME = 'recipe-store';
let db: IDBDatabase;

@Injectable({
  providedIn: 'root'
})
export class IndexedDbService {

    openDatabase(): Promise<IDBDatabase> {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(DB_NAME, DB_VERSION);
  
        request.onerror = (event) => {
          reject(request.error);
        };
  
        request.onsuccess = (event) => {
          db = request.result;
          resolve(db);
        };
  
        request.onupgradeneeded = (event: any) => {
          db = event.target.result;
          const objectStore = db.createObjectStore(OBJECT_STORE_NAME, { keyPath: 'id', autoIncrement: true });
          //objectStore.add();
        };
      });
    }
  
    addToObjectStore(data: any): Promise<number> {
      return new Promise((resolve, reject) => {
        const transaction = db.transaction([OBJECT_STORE_NAME], 'readwrite');
        const objectStore = transaction.objectStore(OBJECT_STORE_NAME);
        const request = objectStore.put(data);
        request.onerror = (event) => {
          console.log(data);

          reject(request.error);
          console.log('Error while adding to db: ', request.error)
        };
  
        request.onsuccess = (event) => {
          const id = request.result;
          resolve(+id);
          console.log('Successfully added something: ', id)
        };
      });
    }
  
    getAllFromObjectStore(): Promise<any[]> {
      return new Promise((resolve, reject) => {
        const transaction = db.transaction([OBJECT_STORE_NAME], 'readonly');
        const objectStore = transaction.objectStore(OBJECT_STORE_NAME);
        const request = objectStore.getAll();
  
        request.onerror = (event) => {
          reject(request.error);
          console.log('Error while getting: ', request.error)
        };
  
        request.onsuccess = (event) => {
          const data = request.result;
          resolve(data);
          console.log('Successfully displayed something: ', data)

        };
      });
    }
  }
