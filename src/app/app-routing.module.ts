import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { AddRecipeComponent } from './components/add-recipe/add-recipe.component';
import { AddClientComponent } from './components/add-client/add-client.component';
import { ViewRecipeComponent } from './components/view-recipe/view-recipe.component';
import { ViewClientComponent } from './components/view-client/view-client.component';

const routes: Routes = [
  {path: '', redirectTo: "landing-page", pathMatch: 'full'},
  {path: "landing-page", component: LandingPageComponent},
  {path: "add-recipe", component: AddRecipeComponent},
  {path: "view-recipe", component: ViewRecipeComponent},
  {path: "add-client", component: AddClientComponent},
  {path: "view-client", component: ViewClientComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
