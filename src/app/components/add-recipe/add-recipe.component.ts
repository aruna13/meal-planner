import { Component } from '@angular/core';
import { Recipe } from 'src/app/models/Recipe';
import { IndexedDbService } from 'src/app/services/indexed-db.service';


@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.sass']
})
export class AddRecipeComponent {
  recipeName : string = "";
  shortDescription: string = "";
  selectedMealType: string = "";
  selectedFoodPreference: string[] = [];
  ingredientsList: string = "";

  constructor(private indexDB : IndexedDbService){}

  ngOnInit(): void {
    
  }

  storeData(data: any) {
    this.indexDB.addToObjectStore(data);
  }

  retrieveData() {
    this.indexDB.getAllFromObjectStore().then((data) => {
      console.log(data);
    })
  }
  
  onSave(){
    
    const recipe = new Recipe(this.recipeName, this.shortDescription, this.selectedMealType, this.ingredientsList, this.selectedFoodPreference);
    this.indexDB.openDatabase().then(db => {
      console.log('Database opened: ', db);

      this.storeData(recipe);

      this.retrieveData();
    });

  }

  updateSelectedFoodPreference(event: Event): void{
    const target = event.target as HTMLInputElement;
    if(target.checked){
      this.selectedFoodPreference.push(target.value);
    }else{
      const index = this.selectedFoodPreference.indexOf(target.value);
      this.selectedFoodPreference.splice(index, 1);
    }

  }
}
