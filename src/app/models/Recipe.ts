export class Recipe{
    Name: string;
    ShortDescription: string;
    MealType: string; //change to enum
    MainIngredients: string;
    FoodType: string[]; //change to enum

    constructor(name: string, shortDescription: string, mealType: string, mainIng: string, foodType: string[]){
        this.Name = name;
        this.ShortDescription = shortDescription;
        this.MealType = mealType;
        this.MainIngredients = mainIng;
        this.FoodType = foodType;
    }
}