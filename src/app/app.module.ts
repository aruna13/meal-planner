import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { AddRecipeComponent } from './components/add-recipe/add-recipe.component';
import { AddClientComponent } from './components/add-client/add-client.component';
import { ViewRecipeComponent } from './components/view-recipe/view-recipe.component';
import { ViewClientComponent } from './components/view-client/view-client.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    NavBarComponent,
    AddRecipeComponent,
    AddClientComponent,
    ViewRecipeComponent,
    ViewClientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
